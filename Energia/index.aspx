﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Energia.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Recibos</title>
    <link href="Energia.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        Archivo csv:<asp:FileUpload ID="Filedelimieter" runat="server" /><br />
        Rango de facturacion: <asp:TextBox runat="server" ID="finicio" TextMode="Date" /> : <asp:TextBox runat="server" ID="ffin" TextMode="Date" /><br />
        Valor Vatio: <asp:TextBox runat="server" id="valorvatio"/><br />
        Fecha limite: <asp:TextBox runat="server" TextMode="Date" ID="limite" /><br />
        Macromedidor:<asp:TextBox runat="server" id="macro"/><br />
        Contador:<asp:TextBox runat="server" id="contador"/><br />
        <asp:Button ID="Cargar" runat="server" OnClick="Cargar_Click" Text="Ver Recibos" />
        <hr />
        Descargue el formato <a href="Energia.csv">Aqui</a> en formato csv<br />
        Mas informacion en jab291214@gmail.com
    </form>
    <div>
        <asp:Repeater runat="server" ID="listado">
            <ItemTemplate>
                <div class="principal">
            <div class="anuncios">
                <h4 class="titulo">Anuncios</h4>
                Yeferson Astudillo cel:320 228 7106
            Saldo. Pendiente: $
            <br />
                Total. Facturado: $#
            <br />
                Recudo. Anterior: $ #
            <br />
                Saldo Anterior: # <%# Eval("SALDO") %>
            <br />
                Mi Deuda: <%# Eval("TOTAL") %>$ 
                <br />
                Estimado Usuario verifique su estado de cuenta en Mi Deuda. No. <%=contador.Text %>
            </div>
            <div class="contenido"></div>
            <h3 class="titulo">Macromedidor <%=macro.Text %> Barrio Napoles</h3>
            <div class="descripcion">
                NO. Predio:
            <label runat="server" id="lab_Numero"><%#Eval("ID") %></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Nombre:
            <label runat="server" id="lab_Nombre"><%#Eval("USUARIO") %></label><br />
                Per. Facturado:
            <label id="fechas" runat="server"><%=finicio.Text %> al <%=ffin.Text %></label>&nbsp;&nbsp;&nbsp;  No. Recibo: _________<br />
                <hr />
                <div class="panizq">
                    <label>Lect Anterior:<%#Eval("LECTURA_ANTERIOR") %></label><br />
                    <label>Lect Actual:<%#Eval("LECTURA_ACT") %></label><br />
                    <label>Consumo:<%#Eval("COSUMO") %></label><br />
                    <label>KiloVatio:<%=valorvatio.Text %></label><br />
                    <div class="titulo">
                        Cancele Antes del:<%= limite.Text%><br />
                    </div>
                </div>
                <div class="pander">
                    Consumo: $<%#Eval("CONSUMO") %><br />
                    Deuda: $<%#Eval("DEUDA2") %><br />
                    Interes: $<%#Eval("INTERES") %><br />
                    Anexos:<%#Eval("SERVICIOS") %><br />
                    Lectura:<%#Eval("LECTURA") %><br />
                    Total:<%#Eval("SALDO3") %><br />
                </div>
            </div>
        </div>
                <hr />
            </ItemTemplate>
        </asp:Repeater>
        
    </div>
</body>
</html>
