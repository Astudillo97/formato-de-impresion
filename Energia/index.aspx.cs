﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Energia
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Cargar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Filedelimieter.FileName != "" && Path.GetExtension(Filedelimieter.FileName) == ".csv")
                {
                    string inputContent;
                    using (StreamReader inputStreamReader = new StreamReader(Filedelimieter.PostedFile.InputStream))
                    {
                        inputContent = inputStreamReader.ReadToEnd().Replace("\r", "");
                    }
                    string[] filas = inputContent.Split('\n');
                    List<usuario> usuar = new List<usuario>();
                    for (int i = 1; i < filas.Length - 1; i++)
                    {
                        string[] datos = filas[i].Split(';');
                        if (datos.Length > 0)
                        {
                            if (datos[13]!= "0")
                            {


                                usuar.Add(new usuario
                                {
                                    ID = datos[0],
                                    USUARIO = datos[1],
                                    SALDO = datos[2],
                                    DEUDA = datos[3],
                                    LECTURA_ANTERIOR = datos[4],
                                    LECTURA_ACT = datos[5],
                                    COSUMO = datos[6],
                                    CONSUMO = datos[7],
                                    DEUDA2 = datos[8],
                                    INTERES = datos[9],
                                    LECTURA = datos[10],
                                    SERVICIOS = datos[11],
                                    SALDO3 = datos[12],
                                    TOTAL = datos[13]
                                });
                            }
                        }
                    }
                    listado.DataSource = usuar;
                    listado.DataBind();
                    form1.Visible = false;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
    public class usuario
    {
        public string ID { get; set; }
        public string USUARIO { get; set; }
        public string SALDO { get; set; }
        public string DEUDA { get; set; }
        public string LECTURA_ANTERIOR { get; set; }
        public string LECTURA_ACT { get; set; }
        public string COSUMO { get; set; }
        public string CONSUMO { get; set; }
        public string DEUDA2 { get; set; }
        public string INTERES { get; set; }
        public string LECTURA { get; set; }
        public string SERVICIOS { get; set; }
        public string SALDO3 { get; set; }
        public string TOTAL { get; set; }
    }
}